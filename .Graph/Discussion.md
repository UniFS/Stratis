https://www.phoronix.com/forums/forum/software/general-linux-open-source/1098311-nixos-takes-action-after-1-2gb-s-zfs-encryption-speed-drops-to-200mb-s-with-linux-5-0?p=1098503#post1098503

Quote: "Honestly, I think Redhat's new Filesystems will probably kill ZFS. Redhat has a better reputation than Oracle and corporate users will not want to store important data on some makeshift Linux bridge.

Have you looked into Redhats "New FS". It's not even a filesystem.. it's a terrible "makeshift Linux bridge" using XFS and a daemon. It wouldn't be wise to replace ZFS with something like that. I'm more willing to place my chips in BTRFS unless redhat rethinks that atrocity."